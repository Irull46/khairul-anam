import React from 'react';
import {Image, Text, View} from 'react-native';

const App = () => {
  return (
    <View style={{width: 720, height: 1480, backgroundColor: 'green'}}>
      <Text style={{color: 'white'}}>Assalamu'alaikum</Text>
      <Text style={{color: 'white'}}>Marhaban Ya Ramadhan</Text>
      <Foto />
    </View>
  );
};

const Foto = () => {
  return (
    <Image
      source={{uri: 'https://www.wartasolo.com/wp-content/uploads/2017/05/Kata-Mutiara-Islami-Menyambut-Bulan-Ramadhan.jpeg'}}
      style={{width: 360, height: 240}}
    />
  );
};

export default App;